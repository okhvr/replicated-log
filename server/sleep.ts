export function sleep(delay: string) {
  const randomDelay = randomNumber(parseInt(delay, 10));
    console.log(`With ${randomDelay}ms delay`)
    return new Promise(resolve => setTimeout(resolve, randomDelay));
}

function randomNumber(max: number) {
  const min = max / 2;
  return Math.floor(Math.random() * (max - min + 1) + min);
}