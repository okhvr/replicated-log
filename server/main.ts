import { Application, Router } from "https://deno.land/x/oak@v12.6.2/mod.ts";
import { oakCors } from "https://deno.land/x/cors@v1.2.2/mod.ts";
import { sleep } from "./sleep.ts";
import { sendUpdateToSecondaries, status } from "./send-update-to-secondaries.ts";
import { assert } from "./assert.ts";
import { findFirstDefined } from "./find-first-defined.ts";
import { fetchWithTimeout } from "./fetch-with-timeout.ts";

const port = 8080;
const messages: (string | undefined)[] = [];
const health = {};

const router = new Router();
router
  .get("/", (context) => {
    context.response.body = JSON.stringify(findFirstDefined(messages));
  })
  .post("/", async (context) => {
    const delay: string | undefined = Deno.env.get("DELAY");
    if (delay) {
      await sleep(delay);
    }
  
    const { message, w, index } = await context.request.body("json").value;
    assert(typeof message === 'string', 'Unexpected message');

    const secondaries: string[] | undefined = Deno.env.get("SECONDARIES")?.split(",");
  
    if (secondaries) {
      if (!checkQuorum(secondaries)) {
        context.response.body = JSON.stringify({ message: "No Quorum" });
        context.response.status = 503;
        return;
      }
      // Primary server 
      const indexForSecondaries = messages.push(message) - 1;
      assert(typeof w === 'number' && w > 0 && w - 1 <= secondaries.length, `Unexpected w argument: ${w}`);
      
      await sendUpdateToSecondaries(secondaries, message, w - 1, indexForSecondaries, health);
    } else {
      // Secondary server
      assert(typeof index === 'number', 'Index should be defined for secondary server');
      messages[index] = message;
    }
  
    context.response.body = JSON.stringify({ message });
  })
  .get("/heartbeat", () => {})
  .get("/health", (context) => {
    context.response.body = JSON.stringify(health);
  });

const app = new Application();
app.use(oakCors()); // Enable CORS for All Routes
app.use(router.routes());
app.use(router.allowedMethods());
app.addEventListener("listen", ({ port }) => {
  console.log(`Start listening on: ${port}`);
});

setInterval(async () => {
  const secondaries: string[] | undefined = Deno.env.get("SECONDARIES")?.split(",");
  if (!secondaries || secondaries.length === 0) {
    return
  }
  // Primary server
  await Promise.all(secondaries.map(async (secondary) => {
    const isHealthy = await checkHeartbeat(secondary);
    const heartbeatStatus = isHealthy
      ? status.Healthy
      : health[secondary] === status.Healthy
        ? status.Suspected
        : status.Unhealthy;
    console.log(`Heartbeat for ${secondary}: ${heartbeatStatus}`);
    health[secondary] = heartbeatStatus;
  }))
}, 30_000)

await app.listen({ port });


function checkQuorum(secondaries: string[]) {
  const unhealthyCount = secondaries
    .map(secondary => health[secondary])
    .filter(healthStatus => healthStatus === status.Unhealthy)
    .length;
  return (secondaries.length + 1) / 2 > unhealthyCount;
}

async function checkHeartbeat(serverUrl: string): Promise<boolean> {
  try { 
    await fetchWithTimeout(`${serverUrl}/heartbeat`);
    return true
  } catch (error) { 
    return false
  } 
}