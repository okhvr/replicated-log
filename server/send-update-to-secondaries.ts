export const status = {
  Healthy: "Healthy",
  Suspected: "Suspected",
  Unhealthy: "Unhealthy"
} as const
export type Status = (typeof status)[keyof typeof status]

export function sendUpdateToSecondaries(secondaries: string[], message: string, w: number, index: number, health: Record<string, Status>) {
    const request = {
        method: "POST",
        headers: new Headers({ "content-type": "application/json" }),
        body: JSON.stringify({ message, index })
    };
    return waitForN(secondaries.map(secondary => fetchDataWithRetry(secondary, request, health)), w);
}

async function fetchDataWithRetry(url: string, payload: any, health: Record<string, Status>, maxRetries: number = Infinity): Promise<any> { 
  let retries = 0; 
  let timeout = 1000;
  const maxTimeout = 300_000;

  while (retries < maxRetries) {
    while (health[url] === status.Unhealthy) {
      await delay(1000);
    }
    try { 
      return await fetch(url, payload);
    } catch (error) { 
      console.error(`Error fetching data (retry ${retries + 1}/${maxRetries}): ${error.message}`); 
      retries++; 
      // a delay between retries
      await delay(timeout > maxTimeout ? maxTimeout : timeout);
      timeout *= 2
    } 
  } 
 
  throw new Error(`Failed to fetch data after ${maxRetries} retries`); 
}

function delay(time: number) {
  return new Promise(resolve => setTimeout(resolve, time)); 
}

function waitForN<T>(promises: Promise<T>[], n: number): Promise<void> {
    return new Promise((resolve, reject) => {
      let resolvedCount = 0;
  
      const checkCompletion = () => {
        if (resolvedCount === n) {
          resolve();
        }
      };
  
      promises.forEach((promise) => {
        promise
          .then(() => {
            resolvedCount++;
            checkCompletion();
          })
          .catch((error) => reject(error));
      });
      checkCompletion();
    });
  }