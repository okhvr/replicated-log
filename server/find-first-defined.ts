export function findFirstDefined(messages: (string | undefined) []): string[] {
  const messagesToReturn: string [] = []
  for (const message of messages) {
    if (message === undefined) {
      return messagesToReturn
    }
    messagesToReturn.push(message)
  }
  return messagesToReturn
}
  
  