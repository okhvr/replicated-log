# Replicated log v3
The current iteration provides tunable semi-synchronicity for replication with a retry mechanism that delivers all messages exactly-once in total order.


## Description
Client-Server application based on Deno and Vue.js

Primary server is a simple HTTP server with: 
 - POST method - appends a message into the in-memory list
 - GET method - returns all messages from the in-memory list

Secondary servers are replicated servers of the primary server.

Implementation details:
- after each POST request, the message is replicated on every secondary server;
- client POST request in addition to the message contains write concern parameter `w=1,2,3,..,n`;
- primary server awaits that indicated number of secondaries (`w`) have received a message;
- primary server’s POST request is finished only after receiving ACKs from `w` number of secondaries (blocking replication approach);
- secondary servers have an artificial delay to test that the replication is blocking and emulating replicas inconsistency (and eventual consistency);
- `index` is passed to secondary servers to guarantee the total ordering of messages - total order;
- all messages are present exactly once in the secondary log - deduplication;
- if message delivery fails (due to connection, or internal server error, or secondary is unavailable) the delivery attempts are repeated. __Retry__ is implemented with an unlimited number of attempts and with some “smart” delays logic;
- a heartbeat mechanism to check secondaries’ health status (_Healthy -> Suspected -> Unhealthy_) is available: GET `/health`.
![Alt text](readme-images/hearbeat-example.png)
- quorum is needed for write calls. If there is no quorum the master is switched into read-only mode and doesn't accept messages append requests and returns `503: No Quorum`.
![Alt text](readme-images/hearbeat-example-unhealthy.png)
![Alt text](readme-images/no-quorum-response.png)
- error handling is out of scope;
- HTTP is used for Primary-Secondary communication;
- logging is supported with `console.log`;
- Primary and Secondaries run in Docker.

## Visuals
![Alt text](readme-images/replicated-log-v1.png)

![Alt text](readme-images/replicated-log-v2.png)

![Alt text](readme-images/replicated-log-v3.png)

![Alt text](readme-images/demo.png)

## Installation
1. Install docker

2. Start primary and secondary servers with docker compose:
```shell
cd server
docker compose up
```

3. Start Frontend app:
```shell
cd replicated-log-demo
pnpm install
pnpm dev
```

4. Open http://localhost:5173/ 
