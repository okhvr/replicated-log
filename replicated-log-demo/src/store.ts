import { reactive } from "vue";
import { fetchMessagesFromPrimary, fetchMessagesFromSecondary1, fetchMessagesFromSecondary2 } from '@/api';

export const store: {
  messagesPrimary: string[]
  messagesSecondary1: string[]
  messagesSecondary2: string[]
} = reactive({
  messagesPrimary: [],
  messagesSecondary1: [],
  messagesSecondary2: []
});


export async function updateMessages() {
  store.messagesPrimary = await fetchMessagesFromPrimary()
  store.messagesSecondary1 = await fetchMessagesFromSecondary1()
  store.messagesSecondary2 = await fetchMessagesFromSecondary2()
}