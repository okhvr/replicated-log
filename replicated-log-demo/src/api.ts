import axios from 'axios'

const url1 = 'http://localhost:8080'
const url2 = 'http://localhost:8081'
const url3 = 'http://localhost:8082'

export const fetchMessagesFromPrimary = async (): Promise<string[]> => {
    return fetchMessages(url1)
}

export const fetchMessagesFromSecondary1 = async (): Promise<string[]> => {
    return fetchMessages(url2)
}

export const fetchMessagesFromSecondary2 = async (): Promise<string[]> => {
    return fetchMessages(url3)
}

const fetchMessages = async (url: string): Promise<string[]> => {
    const { data } = await axios.get(url)
    return data
}

export const addMessage = async (message: string): Promise<{message: string}> => {
    return (await axios.post(url1, { message, w: 1 })).data
}
